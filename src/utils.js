export const getStatus = (item) => {
    let statusClass;
    switch (item.status) {
        case 'Выполнено':statusClass = "status done";
            break;
        case 'В процессе': statusClass = "status process";
            break;
        case 'Ожидает выполнения': statusClass = "status wait";
            break;
        default:
            statusClass = "status"
            break;
    }
    return statusClass;

};