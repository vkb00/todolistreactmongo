import './App.css'
import Form from "./components/Form/index.jsx";
import TaskList from "./components/TaskList/index.jsx";
import {useEffect, useState} from "react";

function App() {
    const [listTasks, setListTasks] = useState([]);
    const [showForm, setShowForm] = useState(false);

    const getAllTasks = async () => {
        const response = await fetch('/api/todo/task');
        const data = await response.json();
        setListTasks(data);
    }
    const addTask = async (data) => {

        const response = await fetch('/api/todo/task', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        });
        const currentTask = await response.json();
        setListTasks([...listTasks, currentTask]);
    }
    const deleteTask = async (id) => {
        const response = await fetch(`/api/todo/task/${id}`, {
            method: 'DELETE',
        });
        setListTasks(listTasks.filter(item => item._id !== id));
    }
    const updateTask = async (data, item) => {
        const newData = {
            _id: item._id,
            title: data.title,
            description: data.description,
            status: data.status
        }

        const response = await fetch(`/api/todo/task/${item._id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({newData})
        });
        setListTasks(prevState => {
            const newState = [...prevState];
            const index = newState.findIndex(current => current._id === item._id);
            newState[index] = newData;

            return newState;

        });
    }
    const closeForm = () => {
        setShowForm(false)
    }

    return (
        <>
            <div className="toolContainer">
                {
                    showForm ? <Form submitMethod={addTask} onClose={closeForm} title="Add Task"/> :
                        <button className="addTask" onClick={() => setShowForm(true)}>Add Task</button>
                }
            </div>

            <TaskList listTasks={listTasks} getAllTasks={getAllTasks} deleteTask={deleteTask} updateTask={updateTask}/>

        </>
    )
}

export default App
