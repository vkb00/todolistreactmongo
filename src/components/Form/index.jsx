import {useForm} from "react-hook-form";
import TitleForm from "../TitleFrom/index.jsx";
import DescriptionForm from "../DescriptionFrom/index.jsx";
import StatusForm from "../StatusFrom/index.jsx";
import {useEffect} from "react";
import "./style.css"
const Form = ({submitMethod, title, onClose, item}) => {
    const {
        register,
        handleSubmit,
        reset,
        setValue,
        formState: {errors}
    } = useForm();

    const onSubmit = (data) => {
        if (item) {
            submitMethod(data, item);
            onClose();
        } else
            submitMethod(data);
        reset();
    }
    useEffect(() => {
        if (item) {
            setValue("title", item.title)
            setValue("description", item.description)
            setValue("status", item.status)
        }

    }, []);

    return (
        <form className="form" onSubmit={handleSubmit(onSubmit)}>
            <div className="tool">
                <h2>{title}</h2>
                <button className="close" onClick={onClose}></button>
            </div>
            <TitleForm register={register} title="title" errors={errors} isRequired={true}/>
            <DescriptionForm register={register} title="description" errors={errors} isRequired={true}/>
            <StatusForm register={register} title="status"/>
            <input className="submit" type="submit"/>
        </form>
    )
}
export default Form;