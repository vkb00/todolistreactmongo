import {useEffect, useState} from "react";
import TaskItem from "../TaskItem/index.jsx";
import "./style.css"
const TaskList = ({listTasks, getAllTasks, deleteTask, updateTask}) => {
    useEffect(() => {
        getAllTasks();
    }, []);

    return (
        <div className="listContainer">
            <div className="headersOfItem">
                <p className="Description">Description</p>
                <p className="listTitle">Task Name</p>
                <p className="listStatus">Status</p>
                <div className="listPanel">
                    <p>Edit</p>
                    <p>Delete</p>
                </div>
            </div>

            {listTasks.map((item) => {
                return (
                    <TaskItem key={item._id} item={item} deleteTask={deleteTask} updateTask={updateTask}/>
                )
            })}
        </div>
    )
}
export default TaskList;