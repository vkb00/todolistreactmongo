import "./style.css"
const StatusForm = ({register, title, isRequired}) => {

    return (
        <div className="statusContainer">
            <label>{title}</label>
            <select id="dropdown"  {...register(title, {required: isRequired})} >
                <option value="Выполнено">Выполнено</option>
                <option value="В процессе">В процессе</option>
                <option value="Ожидает выполнения">Ожидает выполнения</option>
            </select>
        </div>
    )
}
export default StatusForm;