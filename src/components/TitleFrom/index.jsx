import "./style.css"
const TitleForm = ({register, title, isRequired,  errors}) => {
    
    return (
        <div className="titleContainer">
            <label>{title}</label>
            <input {...register(title, {required: isRequired})} />
            {errors[title] && <span>Заполните поле!</span>}
        </div>
    )
}
export default TitleForm;