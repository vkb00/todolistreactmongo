import Form from "../Form/index.jsx";
import "./style.css"
const ModalUpdate = ({submitMethod, onClose, item}) => {
    const onSubmit = (data) => {
        submitMethod(data);
    }

    return (
        <div className="modalContainer">
            <Form submitMethod={submitMethod} item={item} title="Edit Task" onClose={onClose}/>
        </div>
    )
}
export default ModalUpdate;