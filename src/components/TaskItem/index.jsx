import {useState} from "react";
import ModalUpdate from "../ModalUpdate/index.jsx";
import {getStatus} from "../../utils.js";
import "./style.css"

const TaskItem = ({item, updateTask, deleteTask}) => {
    const [showDescription, setShowDescription] = useState(false);
    const [showModal, setShowModal] = useState(false);
    const closeModal = () => {
        setShowModal(false);
    }

    return (
        <>
            <div className="taskContainer" key={item._id}>

                <div className="taskContent">
                    <button className="descriptionBtn" onClick={() => setShowDescription(!showDescription)}>
                        <img src={!showDescription ? "../../../public/img/right.png" :
                            "../../../public/img/down.png"}
                             alt="desc"/>
                    </button>
                    <h2 className="title">{item.title}</h2>
                    <p className={getStatus(item)}>{item.status}</p>
                    <div className="panel">
                        <button className="update" onClick={() => setShowModal(true)}></button>
                        <button className="close" onClick={() => deleteTask(item._id)}></button>
                    </div>
                </div>
                {
                    showDescription &&
                    <div className="description">
                        <h3>Description:</h3>
                        <p>{item.description}</p>
                    </div>

                }

            </div>
            {
                showModal && <ModalUpdate submitMethod={updateTask} item={item} onClose={closeModal}/>
            }
        </>

    )
}
export default TaskItem;