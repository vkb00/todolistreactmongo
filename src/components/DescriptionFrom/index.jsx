import "./style.css"
const DescriptionForm = ({register, title, isRequired, maxLength, errors}) => {
    return (
        <div className="descriptionContainer">
            <label>{title}</label>
            <textarea
                rows={3}
                {...register(title, {required: isRequired, maxLength: maxLength})}
            />
            {errors[title] && <span>Заполните поле!</span>}
        </div>
    )
}
export default DescriptionForm;